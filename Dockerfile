FROM python:3.11-bookworm

RUN apt update && apt install -y tini ca-certificates && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

WORKDIR /uploader
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY main.py .
COPY progress.py .

STOPSIGNAL SIGINT
ENTRYPOINT [ "/usr/bin/tini-static", "--", "/uploader/main.py" ]
